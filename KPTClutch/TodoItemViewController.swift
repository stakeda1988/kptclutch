//
//  TodoItemViewController.swift
//  KPTClutch
//
//  Created by SHOKI TAKEDA on 1/2/16.
//  Copyright © 2016 kptclutch.com. All rights reserved.
//

import UIKit
import CoreData

class TodoItemViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    var globalGenre = "keep"
    let texts:NSArray = [ "keep", "problem", "try" ]
    @IBOutlet weak var todoField: UITextView!
    @IBOutlet weak var pickerView: UIPickerView!
    var task: Todo? = nil
    @IBAction func save(sender: UIBarButtonItem) {
        if task != nil {
            editTask()
        } else {
            createTask()
        }
        navigationController!.popViewControllerAnimated(true)
    }
    func createTask() {
        let newTask: Todo = Todo.MR_createEntity() as! Todo
        newTask.item = todoField.text
        newTask.managedObjectContext!.MR_saveToPersistentStoreAndWait()
    }
    
    func editTask() {
        task?.item = todoField.text
        task?.managedObjectContext!.MR_saveToPersistentStoreAndWait()
    }
    
    @IBAction func cancel(sender: UIBarButtonItem) {
        navigationController!.popViewControllerAnimated(true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        if let taskTodo = task {
            todoField.text = taskTodo.item
        }
        pickerView.delegate = self
        pickerView.dataSource = self
    }
    
    //表示列
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    //表示個数
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return texts.count
    }
    
    //表示内容
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return texts[row] as! String
    }
    
    //選択時
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch row {
            case 0 :
                globalGenre = "keep"
            case 1 :
                globalGenre = "problem"
            case 2 :
                globalGenre = "try"
            default:
                globalGenre = "keep"
        }
        print(globalGenre)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
