//
//  ViewController.swift
//  KPTClutch
//
//  Created by SHOKI TAKEDA on 1/2/16.
//  Copyright © 2016 kptclutch.com. All rights reserved.
//

import UIKit
import CoreData

class TodoListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    var todoEntities = [Todo]()

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        todoEntities = (Todo.MR_findAll() as? [Todo])!
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        todoEntities = (Todo.MR_findAll() as? [Todo])!
        tableView.backgroundView = nil
        tableView.rowHeight = 80
        tableView.backgroundColor = UIColor.clearColor()
        tableView.separatorColor = UIColor.clearColor()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return todoEntities.count
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            todoEntities.removeAtIndex(indexPath.row).MR_deleteEntity()
            NSManagedObjectContext.MR_defaultContext().MR_saveToPersistentStoreAndWait()
            tableView.reloadData()
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("TodoListItem")!
        cell.textLabel!.text = todoEntities[indexPath.row].item
        cell.backgroundColor = UIColor.clearColor()
        let cellSelectedBgView = UIView()
        cellSelectedBgView.backgroundColor = UIColor.clearColor()
        cell.selectedBackgroundView = cellSelectedBgView
        cell.contentView.backgroundColor = UIColor.clearColor()
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.textColor = UIColor.whiteColor()
        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "edit" {
            let todoController = segue.destinationViewController as! TodoItemViewController
            let task = todoEntities[tableView.indexPathForSelectedRow!.row]
            todoController.task = task
        }
    }

    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        tableView.reloadData()
    }
}

